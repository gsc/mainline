import os
import optparse
import subprocess
import ConfigParser
import pkg_resources

import pyparsing
from pyparsing import (
    Group, Literal, ZeroOrMore, printables, Word, restOfLine, Optional,
    White,
    )

import git

def setting(package, name):
    """Read a setting from a configuration file contained in the package.
    Assumes the configuration file is a resource named [package].cfg."""

    config = ConfigParser.ConfigParser()
    config.readfp(
        pkg_resources.resource_stream(package, '%s.cfg' % package))
    return config.get(package, name)

def _make_arg_parser():
    """Create an option parser for the command line interface."""

    parser = optparse.OptionParser(
        usage="%prog [options] [svn repository]",
        version=setting('gsc', 'version'),
        description=setting('gsc', 'description'),)

    parser.add_option('-s', '--standard', dest='std_layout',
                      help='Assume a "standard" Subversion layout.',
                      action='store_true')
    parser.add_option('-n', '--no-history', dest='latest',
                      help='Do not checkout full history, only the latest revision',
                      action='store_true')
    parser.add_option('-v', '--verbose', dest='verbosity',
                      help='Output progress information.',
                      action='count')

    parser.set_defaults(std_layout=False,
                        latest=False,
                        verbosity=0)

    return parser

def parse_externals(externals_info):
    """Given the output from ::

       $ git svn show-externals

    Parse and return a nested list containing the externals definitions."""

    nothash = "".join( [ c for c in printables if c != "#" ] )

    grammer = ZeroOrMore(
        Group(
            Literal("# ").suppress() + restOfLine + Group(
                ZeroOrMore (
                Group(Word(nothash) + Optional(
                        White(" ").suppress() + restOfLine)
                      )
                ))
            )
        )

    return grammer.parseString(externals_info).asList()

def checkout_svn(svn_path, repo_path, std_layout=False, 
                 latest=False):
    """Check out a Subversion repository using git-svn.  Recursively 
    looks at externals and creates shallow checkouts of them using git.
    """

    # make sure the repository path exists
    if not os.path.exists(repo_path):
        os.makedirs(repo_path)

    # initialize the git repository
    git_repo = git.Git(repo_path)
    git_repo.init()

    # initialize the git-svn tracking
    init_args = []
    if std_layout:
        init_args.append('-s')

    init_args.append(svn_path)

    git_repo.svn('init', *init_args)

    # fetch the repository
    fetch_args = []
    if latest:
        # try to get the latest revision from Subversion
        svn_log = subprocess.Popen(
            ['svn','info', svn_path],
            stdout=subprocess.PIPE)

        svn_log.wait()
        if svn_log.returncode == 0:
            # return successfully, look for the last changed rev
            for line in svn_log.stdout.readlines():
                if line.find('Last Changed Rev:') == 0:
                    # found the last revision
                    last_rev = line.rsplit(' ', 1)[-1].strip()
                    fetch_args += ['-r', last_rev]

    git_repo.svn('fetch', *fetch_args)

    # iterate over svn:externals
    for path, exts in parse_externals(git_repo.svn('show-externals')):
        
        try:
            for co_path, svn_path in exts:
                file_path = os.path.abspath(repo_path + co_path)
                checkout_svn(svn_path, file_path, latest=True)
        except IndexError:
            pass

def clone():
    """Check out a subversion repository using git-svn."""

    (opts, args) = _make_arg_parser().parse_args()

    # set verbosity flags
    if opts.verbosity > 0:
        os.environ['GIT_PYTHON_TRACE'] = "True"

    if len(args) == 1:
        # derive the repository directory from the subversion path
        args.append(args[0].split('/')[-1])

    svn_path, repo_path = args[:2]

    checkout_svn(svn_path, repo_path, 
                 std_layout=opts.std_layout, latest=opts.latest)

if __name__ == '__main__':
    clone()

