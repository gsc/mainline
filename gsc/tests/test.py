import unittest
import os
import shutil
import subprocess
import tempfile
import glob

class GscTestCase(unittest.TestCase):

    def __init__(self, *args):
        super(GscTestCase, self).__init__(*args)

        self._rm_dirs = []

    def _tempdir(self):
        """Create a temporary directory and return its name; the name will
        also be recorded for removal in tearDown."""

        tmp = tempfile.mkdtemp()
        self._rm_dirs.append(tmp)

        return tmp

    def _populate_staging(self):

        os.mkdir(os.path.join(self._staging_dir, 'test1'))
        file(os.path.join(self._staging_dir, 'test1', 'test.txt'), 'w').write(
            "12345")

        os.mkdir(os.path.join(self._staging_dir, 'test2'))
        file(os.path.join(self._staging_dir, 'test2', 'test.txt'), 'w').write(
            "abcde")

    def setUp(self):

        """
        # create the directory structure we're going to import
        self._staging_dir = self._tempdir()

        """

        # create an empty svn repository we can use to test with
        self.svn_repo = self._tempdir()
        svnadmin = subprocess.Popen(
            ["svnadmin", "create", self.svn_repo])
        svnadmin.wait()

        # checkout the directory
        self._staging_dir = self._tempdir()
        svn_co = subprocess.Popen(
            ["svn", "co", "file://%s" % self.svn_repo, self._staging_dir])
        svn_co.wait()

        # add any contents to the staging area
        self._populate_staging()

        # commit the staged changed
        svn_add = subprocess.Popen(
            ["svn", "add"] + glob.glob(os.path.join(self._staging_dir, "*"))
            )
        svn_add.wait()

        svn_ci = subprocess.Popen(
            ["svn", "ci", self._staging_dir, "-m", '"Import staging."'])
        svn_ci.wait()

    def tearDown(self):
        
        # remove any temporary directories created
        while self._rm_dirs:
            tmp = self._rm_dirs.pop()

            # shutil.rmtree(tmp)

if __name__ == '__main__':
    unittest.main()
