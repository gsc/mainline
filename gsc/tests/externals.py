import unittest
import os
import shutil
import subprocess
import tempfile
import glob

from test import GscTestCase

class ExternalsCheckoutTest(GscTestCase):

    def _populate_staging(self):

        super(ExternalsCheckoutTest, self)._populate_staging()

        # add test2 as an external on test1
        add = subprocess.Popen(["svn", "add", os.path.join(self._staging_dir, "test1")])
        add.wait()

        propset = subprocess.Popen(["svn", "propset", "svn:externals",
                                    "test2 file://%s/test2" % self.svn_repo,
                                    os.path.join(self._staging_dir, "test1")])
        propset.wait()

    def test_checkout_with_externals(self):
        from gsc.cmd import checkout_svn
        import git

        # checkout the repository using git
        git_dir = self._tempdir()

        checkout_svn("file://%s/test1" % self.svn_repo, git_dir)

        # assert that the files exist
        self.assert_(os.path.exists(os.path.join(git_dir, 'test.txt')))

        # check the contents
        self.assertEqual(
            file(os.path.join(git_dir, 'test.txt'), 'r').read(),
            '12345')

        # make sure test2 was cloned as a submodule
        self.assert_(os.path.exists(os.path.join(git_dir, "test2")))
        self.assert_(os.path.exists(os.path.join(git_dir, "test2", ".git")))
        self.assertEqual(
            file(os.path.join(git_dir, "test2", 'test.txt'), 'r').read(),
            'abcde'
            )        

class ExternalsWithNestedPath(GscTestCase):

    def _populate_staging(self):

        super(ExternalsWithNestedPath, self)._populate_staging()

        # add test2 as an external on test1
        add = subprocess.Popen(["svn", "add", os.path.join(self._staging_dir, "test1")])
        add.wait()

        propset = subprocess.Popen(["svn", "propset", "svn:externals",
                                    "foo/test2 file://%s/test2" % self.svn_repo,
                                    os.path.join(self._staging_dir, "test1")])
        propset.wait()

    def test_checkout_with_externals(self):
        from gsc.cmd import checkout_svn
        import git

        # checkout the repository using git
        git_dir = self._tempdir()

        checkout_svn("file://%s/test1" % self.svn_repo, git_dir)

        # assert that the files exist
        self.assert_(os.path.exists(os.path.join(git_dir, 'test.txt')))

        # check the contents
        self.assertEqual(
            file(os.path.join(git_dir, 'test.txt'), 'r').read(),
            '12345')

        # make sure test2 was cloned as a submodule
        self.assert_(os.path.exists(os.path.join(git_dir, "foo", "test2")))
        self.assert_(os.path.exists(os.path.join(git_dir, "foo", "test2", ".git")))
        self.assertEqual(
            file(os.path.join(git_dir, "foo", "test2", 'test.txt'), 'r').read(),
            'abcde'
            )
        

class ExternalsOnEmptySubDir(GscTestCase):

    def _populate_staging(self):

        GscTestCase._populate_staging(self)

        # create an interstitial directory
        os.mkdir(os.path.join(self._staging_dir, "test1", "externals"))

        # add test2 as an external on test1
        add = subprocess.Popen(["svn", "add", os.path.join(self._staging_dir, "test1")])
        add.wait()

        propset = subprocess.Popen(["svn", "propset", "svn:externals",
                                    "test2 file://%s/test2" % self.svn_repo,
                                    os.path.join(self._staging_dir, 
                                                 "test1", "externals")])
        propset.wait()

    def test_clone(self):
        from gsc.cmd import checkout_svn
        import git

        # checkout the repository using git
        git_dir = self._tempdir()

        checkout_svn("file://%s/test1" % self.svn_repo, git_dir)

        # assert that the files exist
        self.assert_(os.path.exists(os.path.join(git_dir, 'test.txt')))

        # check the contents
        self.assertEqual(
            file(os.path.join(git_dir, 'test.txt'), 'r').read(),
            '12345')

        # make sure test2 was cloned as a submodule
        self.assert_(os.path.exists(os.path.join(git_dir, "externals", "test2")))
        self.assert_(os.path.exists(os.path.join(git_dir, "externals", "test2", ".git")))
        self.assertEqual(
            file(os.path.join(git_dir, "externals", "test2", 'test.txt'), 'r').read(),
            'abcde'
            )
        
