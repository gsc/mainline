import unittest
import discover

import test, clone, externals

def suite():
    """Return a unittest.TestSuite containing the gsc tests."""

    loader = discover.DiscoveringTestLoader()

    return unittest.TestSuite([
            loader.loadTestsFromModule(test),
            loader.loadTestsFromModule(clone),
            loader.loadTestsFromModule(externals),
            ])
