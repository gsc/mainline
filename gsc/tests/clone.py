import unittest
import os
import shutil
import subprocess
import tempfile
import glob

from test import GscTestCase

class CheckoutTest(GscTestCase):

    def test_checkout(self):
        from gsc.cmd import checkout_svn
        import git

        # checkout the repository using git
        git_dir = self._tempdir()

        checkout_svn("file://%s/test1" % self.svn_repo, git_dir)

        # assert that the files exist
        self.assert_(os.path.exists(os.path.join(git_dir, 'test.txt')))

        # check the contents
        self.assertEqual(
            file(os.path.join(git_dir, 'test.txt'), 'r').read(),
            '12345')

    def test_std_layout(self):

        self.assert_(False)
