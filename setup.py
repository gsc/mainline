## Copyright (c) 2009, Nathan R. Yergler <nathan@yergler.net>

## Permission is hereby granted, free of charge, to any person obtaining
## a copy of this software and associated documentation files (the "Software"),
## to deal in the Software without restriction, including without limitation
## the rights to use, copy, modify, merge, publish, distribute, sublicense,
## and/or sell copies of the Software, and to permit persons to whom the
## Software is furnished to do so, subject to the following conditions:

## The above copyright notice and this permission notice shall be included in
## all copies or substantial portions of the Software.

## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
## FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
## DEALINGS IN THE SOFTWARE.

import os
from setuptools import setup
import pkg_resources
import ConfigParser

def setting(package, name):
    """Read a setting from a configuration file contained in the package.
    Assumes the configuration file is a resource named [package].cfg."""

    config = ConfigParser.ConfigParser()
    config.readfp(
        pkg_resources.resource_stream(package, '%s.cfg' % package))
    return config.get(package, name)

def read(*rnames):
    return open(os.path.join(os.path.dirname(__file__), *rnames)).read()

setup(
    name = "gsc",
    version = setting('gsc', 'version'),
    packages = ['gsc'],

    entry_points = {        
        'console_scripts' : [
            'gsc = gsc.cmd:clone',
            ]
        },
    
    install_requires = ['setuptools',
                        'pyparsing',
                        'GitPython',
                        ],

    test_suite = "gsc.tests.suite",
    tests_require = ['discover',
                     ],

    include_package_data = True,
    package_data = {
        '' : ['*.cfg'],
        },
    zip_safe = True,

    author = 'Nathan R. Yergler',
    author_email = 'nathan@yergler.net',
    description = setting('gsc', 'description'),
    long_description = read('README'),
    license = 'BSD',
    url = 'http://gitorious.org/gsc',

    )
